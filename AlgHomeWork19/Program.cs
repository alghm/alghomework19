﻿string source = "Search substring in string";
string search = "str";


var result = FullSearchSubstring(source, search);
var resultOpt = FullSearchSubstringWithPrefix(source, search);
var resultBM = BoyerMooreSearch(source, search);

Console.WriteLine();


static List<int> FullSearchSubstring(string text, string substring)
{
    List<int> indices = new List<int>();
    for (int i = 0; i <= text.Length - substring.Length; i++)
    {
        bool match = true;
        for (int j = 0; j < substring.Length; j++)
        {
            if (text[i + j] != substring[j])
            {
                match = false;
                break;
            }
        }
        if (match)
        {
            indices.Add(i);
        }
    }
    return indices;
}

static List<int> FullSearchSubstringWithPrefix(string text, string substring)
{
    List<int> indices = new List<int>();
    int substringLength = substring.Length;
    int textLength = text.Length;

    for (int i = 0; i <= textLength - substringLength;)
    {
        int j;
        for (j = 0; j < substringLength; j++)
        {
            if (text[i + j] != substring[j])
            {
                break;
            }
        }
        if (j == substringLength)
        {
            indices.Add(i);
            i += j;
        }
        else
        {
            i += j + 1;
        }
    }
    return indices;
}

static int BoyerMooreSearch(string text, string pattern)
{
    int[] shiftTable = BuildShiftTable(pattern);
    int patternLength = pattern.Length;
    int textLength = text.Length;
    int i, j; 
    i = j = patternLength - 1;

    while (i < textLength)
    {
        while (j >= 0 && pattern[j] == text[i])
        {
            i--;
            j--;
        }
        if (j < 0)
        {
            return i + 1;
        }
        else
        {
            i += Math.Max(shiftTable[text[i]], patternLength - j - 1);
        }
    }

    return -1;
}

static int[] BuildShiftTable(string pattern)
{
    int patternLength = pattern.Length;
    int[] shiftTable = new int[256];

    for (int i = 0; i < 256; i++)
    {
        shiftTable[i] = patternLength;
    }

    for (int i = 0; i < patternLength - 1; i++)
    {
        shiftTable[pattern[i]] = patternLength - i - 1;
    }

    return shiftTable;
}